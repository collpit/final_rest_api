
## Requirements

1. Java - 1.8.x

2. Maven - 3.x.x

3. Mysql - 5.x.x

## Steps to Setup


**1. Create Mysql database**
```bash
create database parkandearn_app
```

**2. Change mysql username and password as per your installation**

+ open `src/main/resources/application.properties`

+ change `spring.datasource.username` and `spring.datasource.password` as per your mysql installation

**3. Build and run the app using maven**

```bash
mvn package
java -jar target/easy-notes-1.0.0.jar
```

Alternatively, you can run the app without packaging it using -

```bash
mvn spring-boot:run
```

The app will start running at <http://localhost:8080>.

## Explore Rest APIs
The following APIs you can check -

    GET /api/vehicleparkandearn

    POST /api/vehicleparkandearn

    GET /api/vehicleparkandearn/{id}

    PUT /api/vehicleparkandearn/{id}

    DELETE /api/vehicleparkandearn/{id}


    and


    GET /api/parkandearn

    POST /api/parkandearn

    GET /api/parkandearn/{id}

    PUT /api/parkandearn/{id}

    DELETE /api/parkandearn/{id}

You can test them using postman or any other rest client.
