package com.example.parknearn.controller;

import com.example.parknearn.model.ParkandEarn;
import com.example.parknearn.repository.ParkandEarnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ParkandEarnController {

    @Autowired
    ParkandEarnRepository parkandearnRepository;

    @GetMapping("/parkandearn")
    public List<ParkandEarn> getAllParkandEarns() {
        return parkandearnRepository.findAll();
    }

    @GetMapping("/parkandearn/{parkid}")
    public ResponseEntity<ParkandEarn> getParkandEarnById(@PathVariable(value = "parkid") Long parkandearnId) {
        ParkandEarn parkandearn = parkandearnRepository.findOne(parkandearnId);
        if(parkandearn == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(parkandearn);
    }

    @PostMapping("/parkandearn")
    public ParkandEarn createParkandEarn(ParkandEarn parkandearn) {
        return parkandearnRepository.save(parkandearn);
    }

    @PutMapping("/parkandearn/{parkid}")
    public ResponseEntity<ParkandEarn> updateParkandEarn(@PathVariable(value = "parkid") Long parkandearnId,
                                            ParkandEarn parkandearnDetails) {
        ParkandEarn parkandearn = parkandearnRepository.findOne(parkandearnId);
        if(parkandearn == null) {
            return ResponseEntity.notFound().build();
        }
        parkandearn.setParkingLocation(parkandearnDetails.getParkingLocation());
        parkandearn.setStartDateTime(parkandearnDetails.getStartDateTime());
        parkandearn.setExitDateTime(parkandearnDetails.getExitDateTime());
        parkandearn.setComments(parkandearnDetails.getComments());
        parkandearn.setTotalEarnings(parkandearnDetails.getTotalEarnings());
        parkandearn.setParkingLocationLat(parkandearnDetails.getParkingLocationLat());
        parkandearn.setParkingLocationLong(parkandearnDetails.getParkingLocationLong());
        parkandearn.setMobileNumber(parkandearnDetails.getMobileNumber());
        parkandearn.setStatus(parkandearnDetails.getStatus());
        parkandearn.setPaymentStatus(parkandearnDetails.getPaymentStatus());
        parkandearn.setListedDateTime(parkandearnDetails.getListedDateTime());

        ParkandEarn updatedparkandearn = parkandearnRepository.save(parkandearn);
        return ResponseEntity.ok(updatedparkandearn);
    }

    @DeleteMapping("/parkandearn/{parkid}")
    public ResponseEntity<ParkandEarn> deleteParkandEarn(@PathVariable(value = "parkid") Long parkandearnId) {
        ParkandEarn parkandearn = parkandearnRepository.findOne(parkandearnId);
        if(parkandearn == null) {
            return ResponseEntity.notFound().build();
        }

        parkandearnRepository.delete(parkandearn);
        return ResponseEntity.ok().build();
    }
}
