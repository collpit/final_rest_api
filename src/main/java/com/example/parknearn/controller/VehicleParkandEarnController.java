package com.example.parknearn.controller;

import com.example.parknearn.model.VehicleParkandEarn;
import com.example.parknearn.repository.VehicleParkandEarnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VehicleParkandEarnController {

    @Autowired
    VehicleParkandEarnRepository vehicleparkandearnRepository;

    @GetMapping("/vehicleparkandearn")
    public List<VehicleParkandEarn> getAllVehicleParkandEarns() {
        return vehicleparkandearnRepository.findAll();
    }

    @GetMapping("/vehicleparkandearn/{id}")
    public ResponseEntity<VehicleParkandEarn> getVehicleParkandEarnById(@PathVariable(value = "id") Long vehicleparkandearnId) {
        VehicleParkandEarn vehicleparkandearn = vehicleparkandearnRepository.findOne(vehicleparkandearnId);
        if(vehicleparkandearn == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(vehicleparkandearn);
    }

    @PostMapping("/vehicleparkandearn")
    public VehicleParkandEarn createVehicleParkandEarn(VehicleParkandEarn vehicleparkandearn) {
        return vehicleparkandearnRepository.save(vehicleparkandearn);
    }

    @PutMapping("/vehicleparkandearn/{id}")
    public ResponseEntity<VehicleParkandEarn> updateVehicleParkandEarn(@PathVariable(value = "id") Long vehicleparkandearnId,
                                           VehicleParkandEarn vehicleparkandearnDetails) {
        VehicleParkandEarn vehicleparkandearn = vehicleparkandearnRepository.findOne(vehicleparkandearnId);
        if(vehicleparkandearn == null) {
            return ResponseEntity.notFound().build();
        }
        vehicleparkandearn.setParkandearnid(vehicleparkandearnDetails.getParkandearnid());
        vehicleparkandearn.setVehiclemodel(vehicleparkandearnDetails.getVehiclemodel());

        vehicleparkandearn.setVehicleyear(vehicleparkandearnDetails.getVehicleyear());
        vehicleparkandearn.setVehicleplateno(vehicleparkandearnDetails.getVehicleplateno());
        vehicleparkandearn.setVehiclecolor(vehicleparkandearnDetails.getVehiclecolor());
        vehicleparkandearn.setVehiclepicture(vehicleparkandearnDetails.getVehiclepicture());
        vehicleparkandearn.setVehiclechasisno(vehicleparkandearnDetails.getVehiclechasisno());
        vehicleparkandearn.setKmsdone(vehicleparkandearnDetails.getKmsdone());
        vehicleparkandearn.setFueltype(vehicleparkandearnDetails.getFueltype());
        vehicleparkandearn.setVehicletransmission(vehicleparkandearnDetails.getVehicletransmission());
        vehicleparkandearn.setSeatingcapacity(vehicleparkandearnDetails.getSeatingcapacity());
        vehicleparkandearn.setBagcapacity(vehicleparkandearnDetails.getBagcapacity());
        vehicleparkandearn.setVehiclepriceperkm(vehicleparkandearnDetails.getVehiclepriceperkm());
        vehicleparkandearn.setUserid(vehicleparkandearnDetails.getUserid());
        vehicleparkandearn.setVehicleverified(vehicleparkandearnDetails.getVehicleverified());

        VehicleParkandEarn updatedvehicleparkandearn = vehicleparkandearnRepository.save(vehicleparkandearn);
        return ResponseEntity.ok(updatedvehicleparkandearn);
    }

    @DeleteMapping("/vehicleparkandearn/{id}")
    public ResponseEntity<VehicleParkandEarn> deleteVehicleParkandEarn(@PathVariable(value = "id") Long vehicleparkandearnId) {
        VehicleParkandEarn vehicleparkandearn = vehicleparkandearnRepository.findOne(vehicleparkandearnId);
        if(vehicleparkandearn == null) {
            return ResponseEntity.notFound().build();
        }

        vehicleparkandearnRepository.delete(vehicleparkandearn);
        return ResponseEntity.ok().build();
    }
}
