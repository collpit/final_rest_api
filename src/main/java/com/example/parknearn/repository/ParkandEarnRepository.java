package com.example.parknearn.repository;

import com.example.parknearn.model.ParkandEarn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkandEarnRepository extends JpaRepository<ParkandEarn, Long> {

}
