package com.example.parknearn.repository;

import com.example.parknearn.model.VehicleParkandEarn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleParkandEarnRepository extends JpaRepository<VehicleParkandEarn, Long> {

}
