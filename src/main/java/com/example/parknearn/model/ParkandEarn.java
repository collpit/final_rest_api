package com.example.parknearn.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "parkandearn")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class ParkandEarn {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long parkid;


    private String parkingLocation;

    private String startDateTime;

    private String exitDateTime;

    private String comments;

    private String totalEarnings;

    private String parkingLocationLat;

    private String parkingLocationLong;

    private String mobileNumber;

    private String status;
    // status value can be reserved or unreserved

    private String paymentStatus;
    // payment status can be set or unset

    private String listedDateTime;


    public Long getParkId() {
        return parkid;
    }

    public void setParkId(Long parkid) {
        this.parkid = parkid;
    }

    public void setParkingLocation(String parkingLocation){
      this.parkingLocation = parkingLocation;
    }


    public String getParkingLocation(){
      return parkingLocation;
    }



    public void setStartDateTime(String startDateTime){
      this.startDateTime = startDateTime;
    }

    public String getStartDateTime(){
      return startDateTime;
    }

    public void setExitDateTime(String exitDateTime){
      this.exitDateTime = exitDateTime;
    }

    public String getExitDateTime(){
      return exitDateTime;
    }

    public void setComments(String comments){
      this.comments = comments;
    }

    public String getComments(){
      return comments;
    }

    public void setTotalEarnings(String totalEarnings){
      this.totalEarnings = totalEarnings;
    }

    public String getTotalEarnings(){
      return totalEarnings;
    }

    public void setParkingLocationLat(String parkingLocationLat){
      this.parkingLocationLat = parkingLocationLat;
    }

    public String getParkingLocationLat(){
      return parkingLocationLat;
    }

    public void setParkingLocationLong(String parkingLocationLong){
      this.parkingLocationLong = parkingLocationLong;
    }

    public String getParkingLocationLong(){
      return parkingLocationLong;
    }

    public void setMobileNumber(String mobileNumber){
      this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber(){
      return mobileNumber;
    }

    public void setStatus(String status){
      this.status = status;
    }

    public String getStatus(){
      return status;
    }

    public void setPaymentStatus(String paymentStatus){
      this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatus(){
      return paymentStatus;
    }

    public void setListedDateTime(String listedDateTime){
      this.listedDateTime = listedDateTime;
    }

    public String getListedDateTime(){
      return listedDateTime;
    }

}
