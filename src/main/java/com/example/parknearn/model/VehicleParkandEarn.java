package com.example.parknearn.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vehicleparkandearn")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class VehicleParkandEarn {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String parkandearnid;

    private String vehiclemodel;

    private String vehicleyear;

    private String vehicleplateno;

    private String vehiclecolor;

    private String vehiclepicture;

    private String vehiclechasisno;

    private String kmsdone;

    private String fueltype;

    private String vehicletransmission;

    private String seatingcapacity;

    private String bagcapacity;

    private String vehiclepriceperkm;

    private String userid;

    private String vehicleverified;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParkandearnid() {
        return parkandearnid;
    }

    public void setParkandearnid(String parkandearnid) {
        this.parkandearnid = parkandearnid;
    }

    public String getVehiclemodel() {
        return vehiclemodel;
    }

    public void setVehiclemodel(String vehiclemodel) {
        this.vehiclemodel = vehiclemodel;
    }

    public String getVehicleyear() {
        return vehicleyear;
    }

    public void setVehicleyear(String vehicleyear) {
        this.vehicleyear = vehicleyear;
    }

    public String getVehicleplateno() {
        return vehicleplateno;
    }

    public void setVehicleplateno(String vehicleplateno) {
        this.vehicleplateno = vehicleplateno;
    }
    public String getVehiclecolor() {
        return vehiclecolor;
    }

    public void setVehiclecolor(String vehiclecolor) {
        this.vehiclecolor = vehiclecolor;
    }
    public String getVehiclepicture() {
        return vehiclepicture;
    }

    public void setVehiclepicture(String vehiclepicture) {
        this.vehiclepicture = vehiclepicture;
    }

    public String getVehiclechasisno() {
        return vehiclechasisno;
    }

    public void setVehiclechasisno(String vehiclechasisno) {
        this.vehiclechasisno = vehiclechasisno;
    }
    public String getKmsdone() {
        return kmsdone;
    }

    public void setKmsdone(String kmsdone) {
        this.kmsdone = kmsdone;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getVehicletransmission() {
        return vehicletransmission;
    }

    public void setVehicletransmission(String vehicletransmission) {
        this.vehicletransmission = vehicletransmission;
    }

    public String getSeatingcapacity() {
        return seatingcapacity;
    }

    public void setSeatingcapacity(String seatingcapacity) {
        this.seatingcapacity = seatingcapacity;
    }

    public String getBagcapacity() {
        return bagcapacity;
    }

    public void setBagcapacity(String bagcapacity) {
        this.bagcapacity = bagcapacity;
    }
    public String getVehiclepriceperkm() {
        return vehiclepriceperkm;
    }

    public void setVehiclepriceperkm(String vehiclepriceperkm) {
        this.vehiclepriceperkm = vehiclepriceperkm;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getVehicleverified() {
        return vehicleverified;
    }

    public void setVehicleverified(String vehicleverified) {
        this.vehicleverified = vehicleverified;
    }


}
